# auto_register: false
# frozen_string_literal: true

require "dry/types"
require "dry/struct"

module Types
  include Dry.Types()
end

module Bookshelf
  class Entity < Dry::Struct
  end
end
