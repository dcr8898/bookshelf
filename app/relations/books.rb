# frozen_string_literal: true

module Bookshelf
  module Relations
    class Books < ROM::Relation[:sql]
      # schema :books, infer: true
      schema(:books, infer: true)
    end
  end
end
