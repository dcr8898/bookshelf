# frozen_string_literal: true

module Bookshelf
  module Actions
    module Books
      class Index < Bookshelf::Action
        include Deps[book_repo: "repositories.book"]

        def handle(*, response)

          response.render(view, books: book_repo.all)
        end
      end
    end
  end
end
