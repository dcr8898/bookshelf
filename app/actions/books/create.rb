# frozen_string_literal: true

module Bookshelf
  module Actions
    module Books
      class Create < Bookshelf::Action
        include Deps[
          book_repo: "repositories.book",
          new_view: "views.books.new"
        ]

        params do
          required(:title).filled(:string)
          required(:author).filled(:string)
        end

        def handle(request, response)
          parameters = request.params

          if parameters.valid?
            book_repo.create(parameters)
            response.redirect_to("/books")
          else
            response.status = 422
            response.render(new_view, book: parameters, errors: parameters.errors)
          end
        end
      end
    end
  end
end
