# frozen_string_literal: true

module Bookshelf
  module Actions
    module Home
      class Index < Bookshelf::Action
        def handle(*, response)
          response.render(view)
        end
      end
    end
  end
end
