# frozen_string_literal: true

module Bookshelf
  module Views
    module Books
      class New < Bookshelf::View
        expose :book, default: {}

        expose :error_messages do |errors: {}|
          errors.each_with_object([]) { |(param, messages), a|
            a.concat([param].product(messages))
          }.map { |(param, message)| "#{param.to_s.capitalize} #{message}." }
        end
      end
    end
  end
end
