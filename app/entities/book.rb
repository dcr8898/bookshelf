# frozen_string_literal: true

module Bookshelf
  module Entities
    class Book < Bookshelf::Entity
      attribute :title, Types::String
      attribute :author, Types::String
    end
  end
end
