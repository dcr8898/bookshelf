# frozen_string_literal: true

module Bookshelf
  module Repositories
    class Book < Bookshelf::Repository[:books]
      commands :create, :delete

      def all
        order = proc { title.asc }
        books.order(&order).to_a
      end
    end
  end
end
