# frozen_string_literal: true

module Bookshelf
  class Routes < Hanami::Routes
    root to: "home.index"
    get "/home", to: "home.index"

    get "/books", to: "books.index"
    get "/books/new", to: "books.new"
    post "/books", to: "books.create"
  end
end
