# frozen_string_literal: true

ROM::SQL.migration do
  change do
    create_table :books do
      primary_key :id, :bigint

      column :title, String, null: false
      column :author, String, null: false

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end
end
