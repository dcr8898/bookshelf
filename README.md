# Bookshelf

This is a walk through of the Hanami Bookshelf example (from the 1.3 guides) using Hanami 2.1.0.beta1. Sorry, I can't wait anymore . . .

## Examples Followed

I relied heavily on the examples of Tim Riley's [DecafSucks](https://github.com/decafsucks/decafsucks) example app (escpecially for setting up ROM-rb for persistence) and Brooke Kuhlmann's [Hemo](https://github.com/bkuhlmann/hemo) demo app (for actions, views, specs, etc.). Hemo, which also has setup for ROM-rb, was generated using another project of his, [Hanamismith](https://github.com/bkuhlmann/hanamismith), which is a project generator for Hanami 2. Hanamismith bakes in ROM-rb setup, so if you want to play with Hanami 2.1 beta now, Hanamismith is the way to go.

Thanks to Tim and Brooke for these examples!

## What I did

I wanted to get a feel for Hanami 2, so I basically rebuilt the Hanami 1.3 [Bookshelf](https://guides.hanamirb.org/v1.3/introduction/getting-started/) example app using Hanami 2.1.0.beta1. The function is virtually identical to the original. Aside from updating syntax, I changed some of the specs. The spirit remains the same. ;-)

You can go to the [commit history](https://gitlab.com/dcr8898/bookshelf/-/commits/main?ref_type=heads) (start from August 8) to follow my progress basically file-by-file, including mistakes and mis-steps. :-)
