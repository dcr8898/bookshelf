# frozen_string_literal: true

RSpec.describe Bookshelf::Views::Books::New do
  let(:view) { described_class.new }
  let(:book_entity) { Bookshelf::Entities::Book }

  context "when re-rendering after errors" do
    let(:partial_book) { book_entity.new(title: "Book Without an Author", author: "") }
    subject { view.call(book: partial_book).to_s }

    it "shows a placeholder message" do
      expect(subject).to include('value="Book Without an Author"')
    end
  end

  context "displays errors" do
    let(:empty_book) { book_entity.new(title: "", author: "") }
    let(:errors) { {title: ["must be filled"], author: ["must be filled"]} }
    subject { view.call(book: empty_book, errors: errors).to_s }

    it "for missing inputs" do
      expect(subject).to include("Title must be filled")
      expect(subject).to include("Author must be filled")
    end
  end
end
