# frozen_string_literal: true

RSpec.describe Bookshelf::Views::Books::Index do
  let(:empty_bookshelf) { [] }
  let(:view) { described_class.new }
  let(:book) { Bookshelf::Entities::Book }

  context "when there are no books" do
    let(:books) { empty_bookshelf }

    it "shows a placeholder message" do
      expect(view.call(books:).to_s).to include("<p>Sorry! Your bookshelf is empty! :(</p>")
    end
  end

  context "when there are books" do
    let(:book1) { book.new(title: "Refactoring", author: "Martin Fowler") }
    let(:book2) { book.new(title: "Domain Driven Design", author: "Eric Evans") }
    let(:books) { [book1, book2] }
    let(:rendered_page) { view.call(books:).to_s }

    it "lists all books" do
      expect(rendered_page.scan(/class="book"/).length).to eq(2)
      expect(rendered_page).to include("Refactoring")
      expect(rendered_page).to include("Eric Evans")
    end

    it "hides empty bookshelf message" do
      expect(rendered_page).to_not include("<p>Sorry! Your bookshelf is empty! :(</p>")
    end
  end
end
