# frozen_string_literal: true

RSpec.describe Bookshelf::Actions::Books::Create, :db do
  subject(:action) { described_class.new }
  let(:params) { Hash[title: "Code", author: "Charles Petzold"] }

  context "with valid params" do
    it "redirects to books#index with valid parameters" do
      response = action.call(params)

      expect(response.status).to eq(302)
      expect(response.headers["location"]).to eq("/books")
    end
  end

  context "with invalid params" do
    let(:params) { Hash.new }

    it "returns HTTP client error" do
      response = action.call(params)

      expect(response.status).to eq(422)
    end

    it "reports parameter errors" do
      response = action.call(params)
      errors = response.request.params.errors

      expect(errors.dig(:title)).to eq(["is missing"])
      expect(errors.dig(:author)).to eq(["is missing"])
    end
  end
end
