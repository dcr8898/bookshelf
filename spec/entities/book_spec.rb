# frozen_string_literal: true

RSpec.describe Bookshelf::Entities::Book do
  specify "can be initialized with attributes" do
    subject = Bookshelf::Entities::Book

    book = subject.new(title: "Refactoring", author: "Martin Fowler")

    expect(book.title).to eq("Refactoring")
    expect(book.author).to eq("Martin Fowler")
  end
end
