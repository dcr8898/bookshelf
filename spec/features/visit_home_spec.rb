# frozen_string_literal: true

RSpec.describe "Home page", :web, :db do
  specify "Visiting the home page" do

    visit "/"

    expect(page).to have_content("Welcome to Bookshelf")
  end
end
