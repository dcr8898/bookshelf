# frozen_string_literal: true

RSpec.describe "add a book", :web, :db do
  let(:repository) { Bookshelf::Repositories::Book.new }

  before do
    repository.delete
  end

  it "can create a new book" do
    visit "/books/new"

    within "form#book-form" do
      fill_in "title", with: "Code"
      fill_in "author", with: "Charles Petzold"

      click_button "Create"
    end

    expect(page).to have_current_path("/books")
    within(".book") { expect(page).to have_content("Charles Petzold") }
  end
end
