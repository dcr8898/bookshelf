# frozen_string_literal: true

RSpec.describe "List Books", :web, :db do
  let(:repository) { Bookshelf::Repositories::Book.new }

  before do
    repository.delete
    repository.create(title: "Practical Object-Oriented Design in Ruby", author: "Sandi Metz")
  end

  specify "displays a list of books on the page" do

    visit "/books"

    within "#books" do
      expect(page).to have_selector(".book", count: 1), "Expected to find one book"
      expect(page).to have_content("Practical Object-Oriented Design in Ruby")
      expect(page).to have_content("Sandi Metz")
    end
  end
end
